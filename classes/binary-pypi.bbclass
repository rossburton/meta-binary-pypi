inherit pypi python_pep517 python3native python3-dir setuptools3-base

PYPI_BUILD_TAG ?= ""

# See https://packaging.python.org/en/latest/specifications/platform-compatibility-tags/

PYPI_PYTHON_TAG ?= "py3"

PYPI_ABI_TAG ?= "none"

PYPI_PLATFORM_TAG ?= "manylinux_2_24_${HOST_ARCH}"
PYPI_PLATFORM_TAG:allarch = "any"

# When we say compile, we actually just copy the precompiled wheel
do_compile() {
    install ${WORKDIR}/*.whl ${PEP517_WHEEL_PATH}/
}

def wheel_url(name, version, build_tag, python_tag, abi_tag, platform_tag, d):
    import re
    wheel_parts = {
        tag: re.sub(r'[^\w\d.]+', '_', part, re.UNICODE)
        for tag, part in locals().items()
        if isinstance(part, str)
    }
    wheel_parts['optional_build_tag'] = f'-{wheel_parts["build_tag"]}' if build_tag else ''
    filename = '{name}-{version}{optional_build_tag}-{python_tag}-{abi_tag}-{platform_tag}.whl'\
               .format_map(wheel_parts)
    return f'https://files.pythonhosted.org/packages/{python_tag}/{name[0]}/{name}/{filename}'

def pypi_wheel_url(d):
    return wheel_url(d.getVar("PYPI_PACKAGE"),
                     d.getVar("PV"),
                     d.getVar("PYPI_BUILD_TAG"),
                     d.getVar("PYPI_PYTHON_TAG"),
                     d.getVar("PYPI_ABI_TAG"),
                     d.getVar("PYPI_PLATFORM_TAG"),
                     d)

SRC_URI += "${@pypi_wheel_url(d)};name=wheel-${PYPI_PLATFORM_TAG}"
