SUMMARY = "B2 Command Line Tool"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d80ae759b26208260482177815ebdb71"

inherit allarch binary-pypi

SRC_URI[sha256sum] = "54f375101b88b957ff2dba21dabdc2f25e505134aa6dfbc75aa19d2139600c98"
SRC_URI[wheel-any.sha256sum] = "309ad1d10f8b43dd49f4b39236440433ac78797399fddba5e76c516788f394d6"
