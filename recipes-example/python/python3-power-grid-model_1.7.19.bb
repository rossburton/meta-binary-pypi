SUMMARY = "Python/C++ library for distribution power system analysis"
LICENSE = "MPL-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=104e78d93f1aa193bd9d81584dddaef5"

inherit binary-pypi

SRC_URI[sha256sum] = "faae1b0965b79e0b52c4e19da6f8a51601eb7e539cc907666c091ca1c8b0f022"
SRC_URI[wheel-manylinux_2_24_aarch64.sha256sum] = "69e01eeec042e7868f4b90c3d2bc1082a1551110897bf872ac9fb80d1bc52df1"
SRC_URI[wheel-manylinux_2_24_x86_64.sha256sum] = "403bda2cfba83cf5d7cc7a22d7e2751225b940d86de25b6f89a32203ef8e65fb"
