# meta-binary-pypi

## Introduction

This layer is an example of how to fetch and install _binary_ Python wheels from
PyPi in an OpenEmbedded recipe.

For the general case this is absolutely the wrong thing to do: a recipe that
builds the source from scratch is the right thing to do. Installing a binary
blindly means that you're ultimately trusting the upstream distribution channel
to not be hijacked in some way, you don't know for sure that you can rebuild it
if you need to make any changes, you don't get automatic integration into the
license and source archiving and manifests, and so on.

However, there are a small number Python packages which are very complex to
rebuild but binary wheels have been published.  This class will let you use
those binary wheels to produce packages without having to build the sources.

During the build process it will fetch both the source and binary distributions, so
that the license validation and source archiving will work correctly.

This is a proof of concept and example, I do not endorse it in production use.

## Usage

Inherit the `binary-pypi` class in your recipe.  This in turn inherits the
`pypi` class, so set `PYPI_PACKAGE` if the recipe name isn't simply the PyPi
package name with `python3-` prepended.  The `pypi` class will add the source
distribution to `SRC_URI`, and the `binary-pypi` will add the binary wheel to
`SRC_URI`.

Next, specify the right tags that were used when building the binary wheel. By
default the build tag is unset, the Python tag is `py3`, the ABI tag is `none`,
and the Platform tag is `manylinux_2_24_${HOST_ARCH}` unless the `allarch` class
is inherited in which case it is `any`.  For more details about what these tags
mean see [Python Platform Compatibility Tags].

Finally, specify the source checksums.  There will need to be a
`SRC_URI[sha256sum]` for the source distribution, and a `SRC_URI[wheel-<platform
tag>.sha256sum]` for each of the binary distributions.

## Contributions

This layer is maintained by Ross Burton (<ross.burton@arm.com>).

Please submit any issues or merge requests to [GitLab].

[Python Platform Compatibility Tags]: https://packaging.python.org/en/latest/specifications/platform-compatibility-tags/
[GitLab]: https://gitlab.com/rossburton/meta-binary-pypi
